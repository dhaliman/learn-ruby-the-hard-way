class Animal
end

## Dog is-a Animal
class Dog < Animal
	
	def initialize(name)
		## Dog has-a name		
		@name = name
	end
end

## Cat is-a Animal
class Cat < Animal
	
	def initialize(name)
		## Cat has-a name
		@name = name
	end
end

## Person is-a object
class Person
	
	def initialize(name)
		## Person has-a name
		@name = name
		
		@pet = nil
	end

	attr_accessor :pet
end

## Employee is a Person
class Employee < Person

	def initialize(name, salary)
		## inheriting the name from the super class Person
		super(name)
		## Employee has-a salary
		@salary = salary
	end
end

## Fish is-a object
class fish
end

## Salmon is-a fish
class Salmon < Fish
end

## Halibut is-a fish
class Halibut < Fish
end

## rover is-a Dog
rover = Dog.new('Rover')
## satan is-a Cat
satan = Cat.new('Satan')
## Mary is-a Person
mary = Person.new('Mary')
## Satan is-a Mary's pet
mary.pet = satan
## Frank is-a employee
frank = employee.new("Frank", 120000)
## rover is Frank's pet
frank.pet = rover
## flipper is-a Fish
flipper = Fish.new()
## Crouse is-a Salmon
crouse = Salmon.new()
## Harry is-a Halibut
harry = Halibut.new()
