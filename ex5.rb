my_name = "Manmeet Singh Dhaliwal"
my_age = 21
my_height = 183
my_weight = 65
my_eyes = "Brown"
my_hair = "Black"
my_teeth = "White"


puts "Let's talk about #{my_name}."
puts "He's #{my_height} centimetres tall."
puts "He's #{my_weight} kilos heavy."
puts "Actually that's not too heavy."
puts "He's got #{my_eyes} eyes and #{my_hair} hair."
puts "His teeth are usually #{my_teeth} depending on the coffee."

puts "If I add #{my_age}, #{my_height}, and #{my_weight} I get #{my_age + my_height + my_weight}."
 
